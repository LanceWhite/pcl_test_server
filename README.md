# Postcode Lookup Test Data Server #

We will be using Wiremock to serve up test data for the Postcode Lookup service.

The repository contains the following:

* A run script.

* The standalone Wiremock jar file.

* A set of data mappings for the following scenarios:
    * A valid postcode for a residential area - CT6 7AL (31 addresses).
    * A valid postcode for a commercial area - ME16 0LS (14 addresses).
    * A valid postcode with only one address - SW1A 1AA (Buckingham Palace).
    * A valid postcode with three addresses - W1J 7NT
    * A valid postcode with a large number of addresses - W2 1JB (100 addresses).
    * A valid postcode that will respond with a number of address after a timeout - CT6 7AP. The response will come after 6 seconds, when the default timeout is set to 4 seconds.
    * A valid postcode that does not exist - AA1 1AA
    * An invalid Postcode - sdffdfa
    * The following are added for completeness, but should not be testable from the UI:
        * A response for a request with no request body.


